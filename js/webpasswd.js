/***********************************************************\
 *                  updatePasswordStatus                   *
 *  Atualiza os indicadores de status da senha quanto aos  *
 *  requisitos de complexidade                             *
\***********************************************************/
function updatePasswordStatus() {
	for(i=0; i<arguments.length; i++) {
		var data = arguments[i].split(':');
		var k = document.getElementById('password-' + data[0]);
		if(data[1] == 1) {
			k.setAttribute('class', 'glyphicon glyphicon-ok');
		} else {
			k.setAttribute('class', 'glyphicon glyphicon-remove');
		}
	}
}


/**************************************\
 *  Instala os tratadores de eventos  *
 **************************************/
window.onload = function() {
	// Compatibilidade com navegadores (muito) antigos
	if (!document.getElementById) return;

	var passwordField = document.getElementById('new-password1');
	var modeField1    = document.getElementById('form-mode1');
	var modeField2    = document.getElementById('form-mode2');

	passwordField.onkeyup = function() {
		CheckPasswordPolicy(['old-password', 'new-password1'], [updatePasswordStatus]);
	}

	modeField1.onclick = function() {
		document.getElementById('new-password').style.display  = "inherit";
		document.getElementById('new-password1').disabled = false;
		document.getElementById('new-password2').disabled = false;
	}

	modeField2.onclick = function() {
		document.getElementById('new-password').style.display  = "none";
		document.getElementById('new-password1').disabled = true;
		document.getElementById('new-password2').disabled = true;
	}

	return true;
}

