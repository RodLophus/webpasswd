#!/usr/bin/perl -T

# Versao
my $version = '1.1.12';

use strict;
use warnings;
use Authen::Simple::PAM;
use HTML::Template;
use CGI;
use CGI::Ajax;


# ------------------------------[ Configuracao ]=------------------------------
# Servico do PAM a ser usado para autenticacao
my $pamService = 'login';

# Diretorio base (para facilitar a configuracao dos demais caminhos)
my $baseDir = '/var/www/webpasswd';

# Linha de comando para o programa que troca a senha
# Serao passados os seguintes parametros via entrada padrao:
# - Token de seguranca (vide abaixo)
# - Nome de usuario
# - Senha nova
my %setPasswordCommand = (
	unix    => "/usr/bin/sudo $baseDir/libexec/webpasswd_setpass kerberos > /dev/null 2>&1",
	windows => "/usr/bin/sudo $baseDir/libexec/webpasswd_setpass ads > /dev/null 2>&1"
);

# Token de seguranca, a ser passado para o programa que troca a senha
my $setPasswordToken = 'tjwQUygfVmyxJ3SLwJKdpVwj';

# Arquivo de template
my $templateFilename = "$baseDir/templates/webpasswd.tmpl";

# Requisitos de complexidade para a nova senha: vetor de hashes, com os elementos:
#  - id: identificador do hash (palavra curta composta por letras minusculas, numeros, "-" e "_")
#  - label: texto descrevendo a condicao (aparece na pagina)
#  - expr: expressao de validacao. Trecho de codigo PERL.  Tem acesso as variaveis 
#          $newPassword e $oldPassword.  O status de retorno indica se a senha
#          atende ou nao aa condicao
my @passwordPolicy = (
	{
		id => 'length',
		label => 'Conter pelo menos 8 caracteres de comprimento',
		expr => 'length($newPassword) > 7'
	},
	{
		id => 'history',
		label => 'Ser diferente da senha anterior',
		expr => '$newPassword ne $oldPassword'
	},
	{
		id => 'complexity',
		label => 'Conter pelo menos 1 letra mai&uacute;scula, 1 min&uacute;scula e 1 n&uacute;mero',
		expr => '($newPassword =~ /[A-Z]/) && ($newPassword =~ /[a-z]/) && ($newPassword =~ /[0-9]/)'
	},
);
# -----------------------------------------------------------------------------

$ENV{PATH} = '/usr/bin';

$CGI::LIST_CONTEXT_WARN = 0; 
my $cgi  = CGI->new();

# Gera codigo javascript para a funcao de verificacao de politicas de senha
# (se comunica com o programa principal via Ajax)
my $ajax = CGI::Ajax->new(CheckPasswordPolicy => \&AjaxCheckPasswordPolicy);

print($ajax->build_html($cgi, \&Main));

exit 0;


#########################################################
#                         Main                          #
#  Gera o formulario e processa as entradas do usuario  #
#########################################################
sub Main {
	my @passwordRequirements;
	my $statusMessage = '';
	my %policyStatus;

	my $template = HTML::Template->new(
		filename => $templateFilename,
		die_on_bad_params => 0
	);

	# Le variaveis do formulario
	my $formMode           = $cgi->param('form-mode') || 1;
	my $username           = $cgi->param('username') || '';
	my $oldPassword        = $cgi->param('old-password') || '';
	my $newPassword        = $cgi->param('new-password1') || '';
	my $newPasswordConfirm = $cgi->param('new-password2') || '';
	my $submited           = $cgi->param('submit') ? 1 : 0;

	# Inicializa variaveis de status e valores default para o template
	# (em geral, manter senhas nas clausulas "value" dos campos do formulario
	# nao eh uma pratica recomendada, mas, neste caso e para este publico,
	# a reducao do transtorno justifica o "risco")
	$template->param(
		VERSION          => $version,
		MODE1            => $formMode == 1,
		MODE2            => $formMode == 2,
		USERNAME         => $username,
		OLD_PASSWORD     => $oldPassword,
		NEW_PASSWORD1    => $newPassword,
		NEW_PASSWORD2    => $newPasswordConfirm,
		ERR_USERNAME      => ! $username && $submited,
		ERR_OLD_PASSWORD  => ! $oldPassword && $submited,
		ERR_NEW_PASSWORD1 => ! $newPassword && $submited,
		ERR_NEW_PASSWORD2 => ! $newPasswordConfirm && $submited,
		STATUS            => 0
	);

	if($submited) { # Formulario foi submetido

		# Nome de usuario e senha validos sao requisitos para ambos os modos do formulario
		if(!($username && $oldPassword)) {
			$statusMessage = 'Voc&ecirc; deve preencher todos os campos deste formul&aacute;rio.';
		} elsif(! CheckPassword($username, $oldPassword)) {
			# Tenta autenticar o usuario
			$statusMessage = 'Nome de usuario ou senha atual incorreta.';
			$template->param(OLD_PASSWORD => '', NEW_PASSWORD1 => '', NEW_PASSWORD2 => '', ERR_OLD_PASSWORD => '1', ERR_USERNAME => '1');
		} elsif($formMode == 1) {
			# Testa senha quanto aas politicas de complexidade
			%policyStatus = CheckPasswordPolicy($oldPassword, $newPassword);
			# Verifica preenchimento dos campos adicionais
			if(!($newPassword && $newPasswordConfirm)) {
				$statusMessage = 'Voc&ecirc; deve preencher todos os campos deste formul&aacute;rio.';
			} elsif($newPassword ne $newPasswordConfirm) {
				$statusMessage = 'Os campos "Nova senha" e "Nova senha (confirma&ccedil;&atilde;o) s&atilde;o diferentes.';
				$template->param(NEW_PASSWORD2 => '', ERR_NEW_PASSWORD2 => '1');
			} elsif(! $policyStatus{result}) {
				$statusMessage = 'Sua senha nova n&atilde;o atende aos requisitos de complexidade.';
				$template->param(NEW_PASSWORD1 => '', NEW_PASSWORD2 => '',
					ERR_NEW_PASSWORD1 => '1', ERR_NEW_PASSWORD2 => '1');
			} else {
				# Todos os testes passaram: troca a senha
				$template->param(OLD_PASSWORD => '', NEW_PASSWORD1 => '', NEW_PASSWORD2 => '', STATUS => 1);
				if(! SetNewPassword('windows', $username, $newPassword)) {
					$template->param(STATUS => 0);
					$statusMessage = 'Erro atualizando a senha no sistema Windows.<br>Por favor contacte o administrador do sistema.';
				} elsif(! SetNewPassword('unix', $username, $newPassword)) {
					$template->param(STATUS => 0);
					$statusMessage = 'Erro atualizando a senha no sistema Unix.<br>Por favor contacte o administrador do sistema.';
				} else {
					$statusMessage = 'Senha trocada com sucesso.';
				}
			}
		} elsif($formMode == 2) {
			# Redefine a senha do sistema Windows
			$template->param(OLD_PASSWORD => '', STATUS => 0);
			if(SetNewPassword('windows', $username, $oldPassword)) {
				$template->param(STATUS => 1);
				$statusMessage = 'Senha redefinida com sucesso.';
			} else {
				$statusMessage = 'Erro atualizando a senha no sistema Windows.<br>Por favor contacte o administrador do sistema.';
			}
		}
	}

	# Converte a estrutura retornada por CheckPasswordPolicy
	# em um vetor de hashes compativel com HTML::Template
	foreach(@passwordPolicy) {
		push(@passwordRequirements, {
			ID    => $$_{id},
			LABEL => $$_{label},
			OK    => $policyStatus{policy}{$$_{id}} # 0 -> condicao nao atendida; 1 -> condicao atendida
		});
	}

	$template->param(MESSAGE => $statusMessage);
	$template->param(PASS_REQS => \@passwordRequirements);
	return $template->output;
}


###########################################################
#                   CheckPasswordPolicy                   #
#  Testa se a senha nova esta de acordo com as politicas  #
###########################################################
sub CheckPasswordPolicy {
	my($oldPassword, $newPassword) = @_;
	my %status;
	my $result = 1;

	foreach(@passwordPolicy) {
		my $testResult = eval($$_{expr}) ? '1' : '0';
		$status{$$_{id}} = $testResult;
		$result &= $testResult;
	}

	return (
			result => $result, # 1 -> senha atende a todas as politicas
			policy => \%status # Status individual para cada politica (0 ou 1)
	);
}


##########################################################################
#                        AjaxCheckPasswordPolicy                         #
#  Chama CheckPasswordPolicy, retornando apenas o status das politicas   #
#  individuais, em uma estrutura mais simples (vetor de strings do tipo  #
#  "id_da_politica:status"),  para compatibilidade com javascript        #
##########################################################################
sub AjaxCheckPasswordPolicy {
	my($oldPassword, $newPassword) = @_;
	my %passwordPolicyStatus = CheckPasswordPolicy($oldPassword, $newPassword);
	my @status;

	foreach(keys %{$passwordPolicyStatus{policy}}) {
		push(@status, $_ . ':' . $passwordPolicyStatus{policy}{$_});
	}
	return @status;
}


#####################################################################
#                          CheckPassword                            #
#  Verifica se a senha do usuario esta correta (autentica via PAM)  #
#####################################################################
sub CheckPassword {
	my($username, $password) = @_;
	my $pam = Authen::Simple::PAM->new(service => $pamService);
	
	$pam->authenticate($username, $password);
}


####################################
#          SetNewPassword          #
#  Define a nova senha do usuario  #
####################################
sub SetNewPassword {
	my($system, $username, $password) = @_;

	open(my $pipe , "| $setPasswordCommand{$system}") or return 0;

	print $pipe ("$setPasswordToken\n$username\n$password");

	return close($pipe) ? 1 : 0;
}
