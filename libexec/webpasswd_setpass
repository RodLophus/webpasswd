#!/bin/sh

# Versao
Version='1.1.12'

# Linha de comando para o "adtool"
ADToolCmd='/usr/local/bin/adtool'

# Linha de comando para o "kadmin"
KAdminCmd='/usr/bin/kadmin'

# Token de "seguranca"
SecToken='tjwQUygfVmyxJ3SLwJKdpVwj'

# ---------------------------------------- // ----------------------------------------


##############################################################
#                          Usage                             #
# Exibe informacoes sobre os parametros da linha de comandos #
##############################################################
Usage() {
	ScriptName=`basename $0`
	cat <<EOF
$ScriptName Versao $Version

Sintaxe: $ScriptName <kerberos|ads|all>

   kerberos: Define a senha do usuario no sistema Unix / Kerberos
   ads:      Define a senha do usuario no sistema Windows / ADS
   all:      Define a senha do usuario nos sistemas Kerberos e ADS

   O script espera 3 parametros pela entrada pardrao (um em cada linha):
     token:  Chave de seguranca alfanumerica (definida no corpo do script)
     login:  Login do usuario cuja seja sera definida
     senha:  Nova senha do usuario

EOF
}

# ---------------------------------------- // ----------------------------------------

if [ $# -ne 1 ]; then
	Usage
	exit 1
fi

if [ -z "$UID" ]; then
	UID=`id -u`
fi

if [ "$UID" -ne 0 ]; then
	echo ERRO: Apenas o usuario root pode executar este script.
	exit 1
fi

Scope=$1

if [ "$Scope" != "kerberos" -a "$Scope" != "ads" -a "$Scope" != "all" ]; then
	Usage
	echo "ERRO: escopo invalido ($Scope)."
	echo
	exit 1
fi

read Token
read Login
read Password

if [ "$Token" != "$SecToken" ]; then
	echo ERRO: Chave de seguranca invalida
	exit 1
fi

if ! ( id $Login > /dev/null 2>&1 ); then
	echo ERRO: Usuario nao encontrado: $Login
	exit 1
fi

echo
echo Usuario: $Login
echo
Error=0

if [ "$Scope" = "kerberos" -o "$Scope" = "all" ]; then
	echo '- Escopo: Kerberos'
	Cmd=`$KAdminCmd -kq "cpw -pw $Password $Login"`
	if ( echo $Cmd | grep -q 'changed' ); then
		echo '==> Senha definida com sucesso.'
	else
		echo '==> ERRO definindo a senha.'
		Error=`expr $Error + 10`
	fi
	echo
fi

if [ "$Scope" = "ads" -o "$Scope" = "all" ]; then
	echo '- Escopo: Active Directory'
	if ( $ADToolCmd setpass "`$ADToolCmd search sAMAccountName $Login | awk -F'[=,]' '{print $2}'`" $Password ); then
		echo '==> Senha definida com sucesso.'
	else
		echo '==> ERRO definindo a senha.'
		Error=`expr $Error + 20`
	fi
	echo
fi

if [ "$Error" -ne 0 ]; then
	echo 'ERRO: A troca de senha falhou em pelo menos um sistema.'
	echo
fi

exit $Error
